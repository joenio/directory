help:
	@echo "4two.art directory"

dev:
	gvim .
	xdg-open http://localhost:8000
	python3 -m http.server

serve:
	python3 -m http.server

index.html: .mrconfig
	HTML=`./bin/domains2html` envsubst < $@.template > $@
