# Journal - 4two

## 2024-09-19, compilando RESTful-DOOM

Compilar RESTful-DOOM no Debian testing, os passos abaixo funcionaram
com sucesso.

```sh
git clone https://github.com/mashaal/restful-doom.git
cd restful-doom
./chocpkg/chocpkg/chocpkg install native:autotools
./configure-and-build.sh
sudo apt install doom-wad-shareware
./src/restful-doom -iwad /usr/share/games/doom/doom1.wad -apiport 6666
```

A API RESTful-DOOM esta documentada em `./RAML/doom.raml`.
Versao HTML da API pode ser visualizada em `./RAML/doom_api.html`.
