# Roadmap - 4two

- [x] Write dublang paper for ICLC 2025
  - July 15, 2024 - call for submissions and reviewers
  - July 30, 2024 - detailed call for contributions and templates available
  - September 15, 2024 - deadline for submissions
  - November 1, 2024 - notification of acceptance
  - May 28-30, 2025 - conference dates
