# 4two directory

projects hosted under `*.4two.art`

## build

```sh
make index.html
```

## DNS and domain settings reference

- https://docs.gandi.net/en/domain_names/faq/record_types/alias_record.html
- https://docs.codeberg.org/codeberg-pages/#personal-(or-organization)-site%2C-third-level-domain
- https://codeberg.page
